#!/usr/bin/env python3

from lcd import lcddriver, i2c_lib
import time

lcd = lcddriver.lcd()

lcd.lcd_display_string("HI THERE", 1)

time.sleep(5)  # sleep 5 seconds

lcd.lcd_clear()

lcd = None



# Installation of iSPRESSO software on Raspberry Pi

## Installation of Raspberry PI / Debian - Raspbian

Get Raspbian on your SD card. This configuration was tested in August 2024 with latest Raspbian (debian bookworm) and python 3.11.2

Get it on your WiFi network.


## Setting up Python 3 and dependencies:

```
sudo apt install python3-rpi-lgpio python3-simplejson python3-flask python3-smbus python3-redis redis-server
```

## Run Raspi-config to enable i2c and 1w:
Run raspi-config as root (sudo) and in the Interfaces menu, enable i2c and then 1w.  Reboot.


Comment out lines in:
/etc/modprobe.d/raspi-blacklist.conf (if any)

To enable temp sensor , add lines to /etc/modules:


```
i2c-bcm2708
```

To enable 1-wire temp sensor, modprobe these, and add to /etc/modules:

```
w1-gpio
w1-therm
```

## Downloading code from GIT:

Create www directory, and clone the code:

```
sudo mkdir /var/www
cd /var/www
,
sudo git clone https://veggiebenz@bitbucket.org/veggiebenz/pyspresso.git .
(the trailing "." is important)

 # create log file with correct permissions
 sudo touch /var/log/ispresso.log 
 sudo chown pi /var/log/ispresso.log
 sudo chgrp pi /var/log/ispresso.log

 # set up the init script
sudo cp ./initscript/ispresso /etc/init.d/
sudo chmod +x /etc/init.d/ispresso
sudo update-rc.d ispresso defaults
```

## Now on to the wiring!

![Fritzing breadboard diagram](http://bitbucket.org/veggiebenz/pyspresso/raw/master/img/ispresso2_bb.png)



